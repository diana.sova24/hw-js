let firstNum = +prompt("Select your first number");
let secondNum = +prompt("Select your second number");
let operation = prompt("What way would you like to calculate numbers? (select from symbols: + , - , * , /)");

function sumNumbers(a, b) {
  sum = a + b;
  return sum;
}

const mathFunc = (a, b) => {
  if (operation === "+") {
    return a + b;
  } else if (operation === "-") {
    return a - b;
  } else if (operation === "*") {
    return a * b;
  } else if (operation === "/") {
    return a / b;
  }
};

const calculate = (a, b, func) => {
  result = func(a, b);
  return result;
}

calculate(firstNum, secondNum, mathFunc);
console.log(result);